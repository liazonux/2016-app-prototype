# App Prototype (2016 UI/UX Bring Your Child to Work Day)

Welcome to the application (app) demo shown at the 2016 UI/UX Bring Your Child to Work Day, demonstrated by the UI/UX team.

Feel free to explore the code, download it, or [Clone](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) the repository and start tinkering.

*Please note that this repository does not accept Pull Requests, and there is no Wiki or Issue Tracker.*

## Getting Started

You'll need a few things to begin exploring and working with the code.

1. A desktop/laptop computer
2. An Integrated Development Environment (IDE)
    * [Sublime Text 3](https://www.sublimetext.com/3)
    * [Visual Studio Code](https://code.visualstudio.com/)
    * [Adobe Brackets](http://brackets.io/)
3. An internet browser
    * [Chrome](https://www.google.com/chrome/browser/desktop/)
    * [Firefox](https://www.mozilla.org/en-US/firefox/new/)
    * [Opera](http://www.opera.com/)

*Optional items:*

* A [Git](https://git-scm.com/) client
    * [SourceTree](https://www.sourcetreeapp.com/)
    * [GitKraken](https://www.gitkraken.com/)
* A [Bitbucket](https://bitbucket.org/) account

-----

## Downloading the Source Code

1. Visit the [Downloads](https://bitbucket.org/liazonux/2016-device-protoype/downloads) section of the repository.
2. Click on the "Download repository" link
3. Save it to a memorable location on your computer
4. Unzip the files into a new folder (ex. 2016-liazon-ux-demo-app)
5. Open an internet browser, and open the index.html file in the browser

You should now be able to browse the app locally on your computer.

### Programming Languages

There are hundreds of programming languages that help us communicate instructions to a computer. This app focuses on languages that let us control what people experience through an internet browser.

Languages used in this app:

* [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
* [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
* [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
    * [jQuery](http://jquery.com/)
    * [jQuery Mobile](http://jquerymobile.com/)

-----

## Glossary

__[Bitbucket](https://bitbucket.org/):__ Bitbucket is a service that hosts your code (repositories), and allows you work with other people to update your code.

__[Git](https://git-scm.com/):__ Git is a free version control system that keeps track of every file in your repository, and every change you've ever made to each file.

__Integrated Development Environment (IDE):__ An IDE is a software application that lets you edit, build, and debug code for many different programming languages.