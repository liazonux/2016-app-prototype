﻿'use strict';

$( document ).on( 'pageinit', function() {
	// Watch the "How Much Sauce?"" radio button list value
	$( 'input[name="radSauceVolume"]' ).change(function() {
		// If it's undefined or "No Sauce"
		if ( $( this ).val() === undefined || $( this ).val() === 'No Sauce' ) {
			// Add a class that hides it
			$( '.js-radSauceVolume' ).addClass( 'd-none' );
		} else {
			// Otherwise, remove the class that hides it
			$( '.js-radSauceVolume' ).removeClass( 'd-none' );
		}
	});
});
